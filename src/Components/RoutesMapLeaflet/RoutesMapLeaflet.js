import React, { useState, useEffect } from 'react'
// import {L, Leaflet} from 'leaflet';
import { Map, TileLayer, Popup, CircleMarker, Tooltip, withLeaflet } from 'react-leaflet'
import Box from '@material-ui/core/Box'
import 'leaflet/dist/leaflet.css';
import { getFilteredRoute, getRouteSegments } from '../../utils';
import { Curve } from 'react-leaflet-curve';

// import styles from './index.module.scss'

// Leaflet.Icon.Default.imagePath =
//     '.leaflet'

// delete Leaflet.Icon.Default.prototype._getIconUrl;

// Leaflet.Icon.Default.mergeOptions({
//     iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
//     iconUrl: require('leaflet/dist/images/marker-icon.png'),
//     shadowUrl: require('leaflet/dist/images/marker-shadow.png')
// });

const CurveWithLeaflet = withLeaflet(Curve);

const RoutesMapLeaflet = (props) => {
	const state = {
		lat: 16.920552,
		lng: 96.156532,
		zoom: 5,
	}

	const {routes, currentCase} = props;

	const [currentRouteData, setCurrentRouteData] = useState(null);
	const [segments, setSegments] = useState(null);

	useEffect(() => {
		if (routes && routes.features && typeof +currentCase == 'number') {
			const filteredRoute = getFilteredRoute(routes,currentCase);
			setCurrentRouteData(filteredRoute);	
		}
	},[currentCase,routes])

	useEffect(() => {
		if (currentRouteData && currentRouteData.length)
			setSegments(getRouteSegments(currentRouteData));
	},[currentRouteData])

	const position = [state.lat, state.lng]
	return (
		<Box display="flex" justifyContent="center" m={2} p={2} id="mainmap">
			<Box p={1} >
				<Map center={position} zoom={state.zoom} style={{ height: '85vh', width: '96vw'}}>
					<TileLayer
						attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
						url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
					/>
					{
						segments && segments.length > 0
						?	segments.map((d,i) => {
								console.log(d);
								return <CurveWithLeaflet positions={d.curvePath} option={{color:'red',dashArray:'5,10'}}  key={"route"+i}/>
								// return (RLCurve as any).withLeaflet(<Curve positions={d.curvePath} color='red' key={"route"+i}/>)
							})
						: null
					}
					{
						segments && segments.length > 0
						? segments.map((d,i) => 
								<CircleMarker center={d.from} color="red" radius={5} fill={true} fillColor="lime" fillOpacity={1.0} key={"locationMarker"+i}>
									<Popup><strong>Location {i+1}:</strong><br/> {d.locationData.properties.description}</Popup>
									<Tooltip permanent><strong>{i+1}</strong></Tooltip>
								</CircleMarker>
							)
						: null
					}			
				</Map>
			</Box>
		</Box>


	)
}

export default RoutesMapLeaflet